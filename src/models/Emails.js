import { DataTypes } from "sequelize";
import sequelize from "../../config/Database.js";
import { User } from "./User.js";
export const Emails = sequelize.define('Emails', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

}, {
    freezeTableName: true,
    timestamps: true,
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    defaultValues: {
        createdAt: DataTypes.NOW,
        updatedAt: DataTypes.NOW,
        deletedAt: null
    }
});

Emails.belongsTo(User, { foreignKey: { name: 'user_id', allowNull: false } });
User.hasMany(Emails, { foreignKey: { name: 'user_id', allowNull: false } });