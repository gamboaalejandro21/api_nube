import { DataTypes } from "sequelize";
import sequelize from "../../config/Database.js";
export const User = sequelize.define('User', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },

}, {
    freezeTableName: true,
    timestamps: true,
    paranoid: true, // opción para habilitar soft deletes
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    defaultValues: {
        createdAt: DataTypes.NOW,
        updatedAt: DataTypes.NOW,
        deletedAt: null
    }
});