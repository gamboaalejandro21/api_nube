import { where } from "sequelize";
import { Emails } from "../models/Emails.js";
import { User } from "../models/User.js";

export async function getDirectories(data) {
    try {
        const users = await User.findAll({
            attributes: ['id', 'name'],
            include: {
                model: Emails,
                attributes: ['email'] // Solo seleccionamos el atributo 'email'
            },
            limit: data.limit,
            offset: data.offset,
        });
        
        // Mapear el resultado para transformar la estructura de Emails a un array de strings
        const result = users.map(user => ({
            ...user.get({ plain: true }), // Convertir Sequelize instance a plain object
            Emails: user.Emails.map(email => email.email) // Transformar el array de objetos Emails a un array de strings
        }));
        
        return result;
    } catch (error) {
        console.log(error);

    }
 
}

export async function createDirectory(data) {
    const newUser = await User.create({ name: data.name });
  
  // Asegurarse de que newUser tiene un id válido antes de intentar crear correos electrónicos
  if (newUser && newUser.id) {
    // Iterar sobre el array de emails y crear un registro de Email para cada uno
    const emailPromises = data.emails.map(email => 
      Emails.create({ email: email, user_id: newUser.id })
    );

    // Esperar a que todos los registros de Email sean creados
    await Promise.all(emailPromises);
  }

  // Devolver el usuario recién creado con sus emails asociados
  return newUser;
}

export async function getDirectory(id) {
    try {

        const users = await User.findAll({
            attributes: ['id', 'name'],
            include: {
                model: Emails,
                attributes: ['email'] // Solo seleccionamos el atributo 'email'
            },
            where: { id: id }
        });
        
        // Mapear el resultado para transformar la estructura de Emails a un array de strings
        const result = users.map(user => ({
            ...user.get({ plain: true }), // Convertir Sequelize instance a plain object
            Emails: user.Emails.map(email => email.email) // Transformar el array de objetos Emails a un array de strings
        }));
        
        return result;
    } catch (error) {
        console.log(error);

    }

}

export async function updateDirectory(id, data) {
    try {

        const user_updated =  await User.update(data, { where: { id: id } });

    } catch (error) {
        console.log(error);
        
    }
}

export async function deleteDirectory(id) {
    return await User.destroy({ where: { id: id } });
}


