import express from 'express';
import { createDirectory, deleteDirectory, getDirectories, getDirectory, updateDirectory } from '../services/CrudServices.js';
import validateSchema from '../validators/validator_body.js';

const mainController = express.Router();


mainController.get('/status/', (req, res) => {
    res.status(200).json('pong')
})

mainController.get('/directories/', (req, res) => {
    getDirectories(req.query).then((data) => {
        res.status(200).json(data)
    }
    ).catch((err) => {
        res.status(500).json(err)
    })
})

mainController.post('/directories/',validateSchema, (req, res) => {
    createDirectory(req.body).then((data) => {
        res.status(200).json(data)
    }
    ).catch((err) => {
        res.status(500
        ).json(err)
    }
    )
})

mainController.get('/directories/:id', (req, res) => {
    getDirectory(req.params.id).then((data) => {
        res.status(200).json(data)
    }
    ).catch((err) => {
        res.status(500).json(err)
    }
    )
})

mainController.put('/directories/:id',validateSchema, (req, res) => {
    updateDirectory(req.params.id, req.body).then((data) => {
        res.status(200).json(data)
    }
    ).catch((err) => {
        res.status(500).json(err)
    })

})

mainController.patch('/directories/:id',validateSchema, (req, res) => {
    updateDirectory(req.params.id, req.body).then((data) => {
        res.status(200).json(data)
    }
    ).catch((err) => {
        res.status(500).json(err)
    })
})

mainController.delete('/directories/:id', (req, res) => {
deleteDirectory(req.params.id).then((data) => {

        res.status(200).json("OK")
    }
    ).catch((err) => {
        res.status(500).json(err)
    })

})

export default mainController;
