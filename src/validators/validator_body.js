import express from 'express';
import { check, validationResult } from "express-validator";

const validateSchema = express.Router();

const schemaValidator = [
    check('name')
        .notEmpty().withMessage('El campo nombre es obligatorio.')
        .isString().withMessage('El campo nombre debe ser una cadena de texto.'),
    check('emails')
        .notEmpty().withMessage('El campo emails es obligatorio.')
        .isArray().withMessage('El campo emails debe ser un arreglo.')
        .custom((emails) => emails.every(email => typeof email === 'string'))
        .withMessage('Todos los elementos de emails deben ser cadenas de texto.'),
    check('emails.*')
        .isEmail().withMessage('Debe ingresar un email válido.')
];

const handleValidation = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            status: 'error',
            message: 'Validación fallida',
            errors: errors.array()
        });
    }
    next();
};

validateSchema.post('/directories/', schemaValidator, handleValidation);
validateSchema.put('/directories/:id', schemaValidator, handleValidation);
validateSchema.patch('/directories/:id', schemaValidator, handleValidation);

export default validateSchema;