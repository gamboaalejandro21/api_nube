import {urls} from './routes.js'
import { Router } from 'express'
const router = Router();

urls.map(url=>{
    router[url.method](url.path,url.middleware || [],url.controller)
})

export default router;