import mainController from "../controllers/MainController.js"
export const urls = [
  // CoverageExtension
  {
    path: '/status/',
    method: 'get',
    controller: mainController,
    // middleware: validate_role("EXTENSIÓN DE COBERTURA","VER")
  },
  {
    path: '/directories/',
    method: 'get',
    controller: mainController,
    // middleware: validate_role("EXTENSIÓN DE COBERTURA","VER")
  },
  {
    path: '/directories/',
    method: 'post',
    controller: mainController,
    // middleware: validate_role("EXTENSIÓN DE COBERTURA","CREAR")
  },
  {
    path: '/directories/:id',
    method: 'get',
    controller: mainController,
    // middleware: validate_role("EXTENSIÓN DE COBERTURA","MODIFICAR")
  },
  {
    path: '/directories/:id',
    method: 'put',
    controller: mainController,
    // middleware: validate_role("EXTENSIÓN DE COBERTURA","ELIMINAR")
  },

  // Entity
  {
    path: "/directories/:id",
    method: "patch",
    controller: mainController,
    // middleware: validate_role("ENTES","VER")
  },
  {
    path: "/directories/:id",
    method: "delete",
    controller: mainController,
    // middleware: validate_role("ENTES","VER")
  },
]