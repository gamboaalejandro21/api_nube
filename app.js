import express from 'express'
import dotenv from 'dotenv';
import morgan from 'morgan';
import cors from 'cors';
import initial from './src/models/index.js';

const app = express();

import router from './src/routes/index.js';
dotenv.config({path: `./deploy/.env.${process.env.NODE_ENV}`});
console.log(process.env['PORT'])
//utilities and middlewares
app.use(cors())
app.use(morgan('dev'));
app.use(express.json({limit: '10mb'}));
app.use(express.urlencoded({ extended: true, limit: '10mb' }));

//routes  
app.use("/", router);
 
app.listen( process.env['PORT'], async () => {
    console.log('Servidor corriendo en el puerto', process.env['PORT'])
})